### README ###

1. Open R in the terminal or RStudio from the working directory where server.R and ui.R are saved.
2. Check packages are installed:
install.packages("shiny")
install.packages("jpeg")
3. Load packages and source plant database information
library(shiny)
library(jpeg)
source("plantInfo.r")
4. In RStudio, press the RunApp button OR from the terminal type runApp()


Contact: kiriwhan@gmail.com
